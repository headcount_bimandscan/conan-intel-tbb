#!/bin/sh

#
# 2018-2019 � BIM & Scan Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "tbb/2019_U2@bimandscan/stable"
