# BIM & Scan� Third-Party Library (Intel TBB)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for [TBB](https://www.threadingbuildingblocks.org), Intel's widely-used C++ template library for task parallelism.

Supports release 2019_U2 (stable).
