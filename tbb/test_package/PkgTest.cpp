/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>
#include <vector>
#include <numeric>

#include <tbb/tbb.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'TBB' package test (compilation, linking, and execution).\n";

    tbb::task_scheduler_init task_sched_init(2);
    static const std::size_t vec_size = 50;

    std::vector<int> int_vec(vec_size);

    std::iota(int_vec.begin(),
              int_vec.end(),
              1);

    tbb::parallel_for(tbb::blocked_range<std::size_t>(0,
                                                      vec_size),
                      [&int_vec](const tbb::blocked_range<std::size_t>& p_range) -> void
                      {
                          for(auto iter = p_range.begin(); iter != p_range.end(); ++iter)
                          {
                               int_vec[iter] *= 3;
                          }
                      });

    std::cout << "'TBB' package works!" << std::endl;
    return EXIT_SUCCESS;
}
