#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile


class TBB(ConanFile):
    name = "tbb"
    version = "2019_U2"
    license = "Apache-2.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-intel-tbb"
    description = "Intel's widely-used C++ template library for task parallelism."
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    generators = "cmake"
    homepage = "https://www.threadingbuildingblocks.org"

    _src_dir = "tbb_src"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "fPIC=True"

    exports = "../LICENCE.md"
    exports_sources = "CMakeLists.txt"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        git_uri = "https://github.com/wjakob/tbb.git" # using unofficial CMake-compatible build for convenience

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "b066defc0229a1e92d7a200eb3fe0f7e35945d95") # latest commit -> 12/11/2018

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["TBB_BUILD_SHARED"] = True
        cmake.definitions["TBB_BUILD_STATIC"] = False
        cmake.definitions["TBB_BUILD_TBBMALLOC"] = True
        cmake.definitions["TBB_BUILD_TBBMALLOC_PROXY"] = True
        cmake.definitions["TBB_BUILD_TESTS"] = False

        if self.settings.os != "Windows":
            cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = self.configure_cmake()
        cmake.install()

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.append("/openmp")
            self.cpp_info.cflags.append("/openmp")
        else:
            self.cpp_info.cppflags.append("-fopenmp")
            self.cpp_info.cflags.append("-fopenmp")

        if self.settings.os != "Windows":
            self.cpp_info.libs.append("pthread")
            self.cpp_info.cppflags.append("-pthread")
            self.cpp_info.cflags.append("-pthread")

        if self.settings.os == "Linux":
            self.cpp_info.libs.extend([
                                          "rt",
                                          "dl"
                                      ])

            if self.settings.compiler == "gcc":
                self.cpp_info.libs.append("m")
